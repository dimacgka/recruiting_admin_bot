package connection

import (
	"dnaim_admin/config"
	naim "dnaim_admin/internal/naim/delivery"
	"dnaim_admin/internal/naim/repository"
	"dnaim_admin/internal/naim/usecase"
	"dnaim_admin/pkg/logger"
	"dnaim_admin/pkg/storage/relational"
)

func (t *TelegramBot) MapHandlers(cfg *config.Config) error {
	logger := logger.InitLogger(cfg)

	//dHttpClient := dhttp.NewHttpClient(cfg, logger)

	naimGoDB := relational.InitPostgres(cfg, logger)
	naimGoRepo := repository.NewNaimGoRepo(naimGoDB, logger)

	naimUseCase := usecase.NewNaimUseCase(naimGoRepo, *logger, cfg)

	clientStatisticHandler := naim.NewNaimHandler(naimUseCase, t.core, *cfg)
	clientStatisticHandler.Attach()

	return nil
}
