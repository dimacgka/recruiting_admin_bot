package connection

import (
	"dnaim_admin/config"
	"fmt"
	tele "gopkg.in/telebot.v3"
	"log"
	"time"
)

type TelegramBot struct {
	core *tele.Bot
	cfg  *config.Config
}

func NewTelegramBot(cfg *config.Config) *TelegramBot {
	settings := tele.Settings{
		Token: cfg.Bot.Token,
		Poller: &tele.LongPoller{
			Timeout: time.Duration(cfg.Bot.TimeOut) * time.Second,
		},
	}
	core, err := tele.NewBot(settings)
	if err != nil {
		panic(fmt.Errorf("can't create new telegram bot: %s", err.Error()))
	}

	bot := &TelegramBot{
		core: core,
		cfg:  cfg,
	}
	err = bot.run()
	if err != nil {
		panic(fmt.Errorf("can't run new telegram bot: %s", err.Error()))
	}

	return bot
}

func (t TelegramBot) run() error {
	if err := t.MapHandlers(t.cfg); err != nil {
		return err
	}
	t.core.Start()
	log.Println("Bot is started!")

	return nil
}

func (t TelegramBot) Shutdown() {
	t.core.Stop()
}
