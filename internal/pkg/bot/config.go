package config

type BotConfig struct {
	Token      string `json:"token"`
	AppVersion string `json:"appVersion"`
	TimeOut    int    `json:"timeOut"`
}

func (bc *BotConfig) ParseConfig() error {
	return nil
}
