package model

import "time"

type GetInterviewByDateParams struct {
	Status            *string `db:"status"`
	InterviewID       *int64  `db:"id"`
	Slot              *string `db:"slot"`
	Interviewer       *string `db:"interviewer"`
	CandidateName     *string `db:"candidate_name"`
	CandidateChatName *string `db:"candidate_chat_name"`
	DateInterview     *string `db:"date_interview"`
	IsHeld            *bool   `db:"is_held"`
	IsApprove         *bool   `db:"is_approve"`
	Job               *string `db:"job"`
	Description       *string `db:"description"`
}

type SetInterviewerParams struct {
	Interviewer   *string    `db:"chat_name"`
	CandidateName *string    `db:"candidate_name"`
	DateInterview *time.Time `db:"date_interview"`
}

type GetStatusResult struct {
	Status *string `db:"status"`
}

type SetStatusParams struct {
	InterviewID *int64  `db:"id"`
	Status      *string `db:"name"`
}

type GetInterviewer struct {
	Name *string `db:"name"`
}
