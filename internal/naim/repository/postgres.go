package repository

import (
	naimInterface "dnaim_admin/internal/naim/interface"
	"dnaim_admin/internal/naim/model"
	"dnaim_admin/pkg/logger"
	"github.com/jmoiron/sqlx"
	"time"
)

type NaimGoRepo struct {
	db     *sqlx.DB
	logger *logger.Logger
}

func NewNaimGoRepo(db *sqlx.DB, logger *logger.Logger) naimInterface.NaimRepositoryI {
	return &NaimGoRepo{
		db:     db,
		logger: logger,
	}
}

func (ngr *NaimGoRepo) GetInterviewByDate(dateFrom, dateTo *time.Time) ([]model.GetInterviewByDateParams, error) {
	query := `
	SELECT s.name AS slot, i.id AS id, COALESCE(i2.name, '') AS interviewer, i.candidate_name, st.name AS status, COALESCE(i.candidate_chat_name, '') AS candidate_chat_name,
	       i.date_interview::DATE::VARCHAR, i.is_held, i.is_approve, COALESCE(i.job, '') AS job, COALESCE(i.description, '') AS description
	FROM interview i
	JOIN slots s ON i.slots_id = s.id
	JOIN status st ON i.status_id = st.id
	LEFT JOIN interviewer i2 ON i.interviewer_id = i2.id
	WHERE date_interview::DATE BETWEEN $1::DATE AND $2::DATE
	ORDER BY date_interview;`

	var data []model.GetInterviewByDateParams

	if err := ngr.db.Select(&data, query, dateFrom, dateTo); err != nil {
		ngr.logger.Errorf("GetInterviewByDate: %v", err)
		return nil, err
	}

	return data, nil
}

func (ngr *NaimGoRepo) SetInterviewer(params *model.SetInterviewerParams) error {
	query := `
	UPDATE interview 
	SET interviewer_id = (SELECT id FROM interviewer WHERE chat_name = $1)
	WHERE candidate_name = $2 AND date_interview = $3;`

	_, err := ngr.db.Exec(query, params.Interviewer, params.CandidateName, params.DateInterview)
	if err != nil {
		ngr.logger.Errorf("SetInterviewer: %v", err)
		return err
	}

	return nil
}

func (ngr *NaimGoRepo) GetInterviewByID(id *int64) (*model.GetInterviewByDateParams, error) {
	query := `
	SELECT s.name AS slot, i.id AS id, COALESCE(i2.name, '') AS interviewer, i.candidate_name, st.name AS status, COALESCE(i.candidate_chat_name, '') AS candidate_chat_name,
	       i.date_interview::DATE::VARCHAR, i.is_held, i.is_approve, COALESCE(i.job, '') AS job, COALESCE(i.description, '') AS description
	FROM interview i
	JOIN slots s ON i.slots_id = s.id
	JOIN status st ON i.status_id = st.id
	LEFT JOIN interviewer i2 ON i.interviewer_id = i2.id
	WHERE i.id = $1;`

	var data []model.GetInterviewByDateParams

	if err := ngr.db.Select(&data, query, id); err != nil {
		ngr.logger.Errorf("GetInterviewByID: %v", err)
		return nil, err
	}

	return &data[0], nil
}

func (ngr *NaimGoRepo) GetStatus() ([]model.GetStatusResult, error) {
	query := `SELECT name AS status FROM status;`

	var data []model.GetStatusResult

	err := ngr.db.Select(&data, query)
	if err != nil {
		ngr.logger.Errorf("GetStatus: %v", err)
		return nil, err
	}

	return data, nil
}

func (ngr *NaimGoRepo) SetStatus(params *model.SetStatusParams) error {
	query := `
	UPDATE interview
	SET status_id = (SELECT id FROM status WHERE name = $1)
	WHERE id = $2;`

	_, err := ngr.db.Exec(query, params.Status, params.InterviewID)
	if err != nil {
		ngr.logger.Errorf("SetStatus: %v", err)
		return err
	}

	return nil
}

func (ngr *NaimGoRepo) GetInterviewer() ([]model.GetInterviewer, error) {
	query := `SELECT name FROM interviewer;`

	var data []model.GetInterviewer

	err := ngr.db.Select(&data, query)
	if err != nil {
		ngr.logger.Errorf("GetStatus: %v", err)
		return nil, err
	}

	return data, nil
}

func (ngr *NaimGoRepo) SetInterviewerByName(params *model.SetInterviewerParams) error {
	query := `
	UPDATE interview 
	SET interviewer_id = (SELECT id FROM interviewer WHERE name = $1)
	WHERE candidate_name = $2 AND date_interview = $3;`

	_, err := ngr.db.Exec(query, params.Interviewer, params.CandidateName, params.DateInterview)
	if err != nil {
		ngr.logger.Errorf("SetInterviewer: %v", err)
		return err
	}

	return nil
}
