package naimInterface

import (
	"dnaim_admin/internal/naim/model"
	"time"
)

type NaimRepositoryI interface {
	GetInterviewByDate(dateFrom, dateTo *time.Time) ([]model.GetInterviewByDateParams, error)
	SetInterviewer(params *model.SetInterviewerParams) error
	GetInterviewByID(id *int64) (*model.GetInterviewByDateParams, error)
	GetStatus() ([]model.GetStatusResult, error)
	SetStatus(params *model.SetStatusParams) error
	GetInterviewer() ([]model.GetInterviewer, error)
	SetInterviewerByName(params *model.SetInterviewerParams) error
}
