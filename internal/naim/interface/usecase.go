package naimInterface

import (
	"dnaim_admin/internal/naim/model"
	tele "gopkg.in/telebot.v3"
	"time"
)

type NaimUseCaseI interface {
	GetInterviewByDate(dateFrom *time.Time, dateTo *time.Time) (*string, error)
	GetInterviewerByDate(dateFrom, dateTo *time.Time) ([]model.GetInterviewByDateParams, error)
	SetInterviewerByDate(params *model.SetInterviewerParams) error
	GetInterviewByID(id *int64) (*string, error)
	GetStatus() ([]model.GetStatusResult, error)
	SetStatus(params *model.SetStatusParams) error
	GetInterviewer() (*tele.ReplyMarkup, error)
	SetInterviewerByName(params *model.SetInterviewerParams) error
}
