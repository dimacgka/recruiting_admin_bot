package naim

import (
	"dnaim_admin/config"
	naimInterface "dnaim_admin/internal/naim/interface"
	"dnaim_admin/internal/naim/model"
	"fmt"
	"golang.org/x/exp/slices"
	tele "gopkg.in/telebot.v3"
	"log"
	"strconv"
	"strings"
	"time"
)

const (
	layout = "02.01.2006"
)

type NaimHandler struct {
	useCase naimInterface.NaimUseCaseI
	kernel  *tele.Bot
	cfg     config.Config
}

func NewNaimHandler(useCase naimInterface.NaimUseCaseI, kernel *tele.Bot, cfg config.Config) NaimHandler {
	return NaimHandler{
		useCase: useCase,
		kernel:  kernel,
		cfg:     cfg,
	}
}

func (nh *NaimHandler) Attach() {
	//---------------------see_interview_by_date-----------------------
	nh.kernel.Handle("/see_interview_by_date", func(ctx tele.Context) error {
		username := ctx.Chat().Username
		if !slices.Contains(nh.cfg.Whitelist, username) {
			return ctx.Send("You are not in white list!")
		}

		err := ctx.Send("Введите дату за которую вы хотите получить собеседования\nВ формате ДД.ММ.ГГГГ ;)")
		if err != nil {
			log.Fatal(err)
			return err
		}

		nh.kernel.Handle(tele.OnText, func(ctx tele.Context) error {
			selectedDateString := strings.Replace(ctx.Text(), "\f", "", 1)
			selectedDateTime, err := time.Parse(layout, selectedDateString)
			if err != nil {
				err = ctx.Send("Неверный формат даты!")
			}

			message, err := nh.useCase.GetInterviewByDate(&selectedDateTime, &selectedDateTime)
			err = ctx.Send(*message, &tele.SendOptions{ParseMode: tele.ModeHTML})
			if err != nil {
				log.Fatal(err)
				return err
			}

			return nil
		})

		return nil
	})

	//---------------------make_interviewer_by_date-----------------------
	nh.kernel.Handle("/make_interviewer_by_date", func(ctx tele.Context) error {
		username := ctx.Chat().Username
		if !slices.Contains(nh.cfg.Whitelist, username) {
			return ctx.Send("You are not in white list!")
		}

		err := ctx.Send("Введите дату за которую вы хотите назначать собеседования\nВ формате ДД.ММ.ГГГГ ;)")
		if err != nil {
			log.Fatal(err)
			return err
		}

		nh.kernel.Handle(tele.OnText, func(ctx tele.Context) error {
			selectedDateString := strings.Replace(ctx.Text(), "\f", "", 1)
			selectedDateTime, err := time.Parse(layout, selectedDateString)
			if err != nil {
				err = ctx.Send("Неверный формат даты!")
			}

			interviews, err := nh.useCase.GetInterviewerByDate(&selectedDateTime, &selectedDateTime)
			var keys []tele.Row
			var keyboard tele.ReplyMarkup

			for i := 0; i < len(interviews); i += 3 {
				var btn []tele.Btn
				for j := i; j < i+3; j++ {
					if j >= len(interviews) {
						break
					}
					btn = append(btn, keyboard.Data(*interviews[j].CandidateName, *interviews[j].CandidateName))
				}
				keys = append(keys, keyboard.Row(btn[:]...))
			}

			keyboard.Inline(keys[:]...)
			err = ctx.Send("Выберите , кандидата которому хотите провести собеседование:", &keyboard)
			if err != nil {
				log.Fatal(err)
			}

			nh.kernel.Handle(tele.OnCallback, func(ctx tele.Context) error {
				candidate := strings.Replace(ctx.Callback().Data, "\f", "", 1)
				err := nh.useCase.SetInterviewerByDate(
					&model.SetInterviewerParams{
						DateInterview: &selectedDateTime,
						Interviewer:   &username,
						CandidateName: &candidate,
					})
				if err != nil {
					return err
				}
				err = ctx.Send(fmt.Sprintf("Кандидату %s успешно назначен интервьювером %s", candidate, username))
				if err != nil {
					log.Fatal(err)
					return err
				}
				return nil
			})

			return nil
		})

		return nil
	})

	//---------------------set_status_by_id-----------------------
	nh.kernel.Handle("/set_status_by_id", func(ctx tele.Context) error {
		username := ctx.Chat().Username
		if !slices.Contains(nh.cfg.Whitelist, username) {
			return ctx.Send("You are not in white list!")
		}

		err := ctx.Send("Введите ID собеседования, чтобы получить информацию и проставить статус по кандидату")
		if err != nil {
			log.Fatal(err)
			return err
		}

		nh.kernel.Handle(tele.OnText, func(ctx tele.Context) error {
			interviewIdString := strings.Replace(ctx.Text(), "\f", "", 1)
			interviewID, err := strconv.ParseInt(interviewIdString, 10, 64)
			message, err := nh.useCase.GetInterviewByID(&interviewID)
			*message += "\n\nПроставьте статус!"
			statuses, err := nh.useCase.GetStatus()
			if err != nil {
				return err
			}
			var keys []tele.Row
			var keyboard tele.ReplyMarkup

			for i := 0; i < len(statuses); i += 3 {
				var btn []tele.Btn
				for j := i; j < i+3; j++ {
					if j >= len(statuses) {
						break
					}
					btn = append(btn, keyboard.Data(*statuses[j].Status, *statuses[j].Status))
				}
				keys = append(keys, keyboard.Row(btn[:]...))
			}

			keyboard.Inline(keys[:]...)
			err = ctx.Send(*message, &keyboard)
			if err != nil {
				log.Fatal(err)
				return err
			}

			nh.kernel.Handle(tele.OnCallback, func(ctx tele.Context) error {
				status := strings.Replace(ctx.Callback().Data, "\f", "", 1)
				err := nh.useCase.SetStatus(
					&model.SetStatusParams{
						InterviewID: &interviewID,
						Status:      &status,
					})
				if err != nil {
					return err
				}
				err = ctx.Send(fmt.Sprintf("Кандидату успешно проставлен статус: %s", status))
				if err != nil {
					log.Fatal(err)
					return err
				}
				return nil
			})

			return nil
		})

		return nil
	})

	//---------------------see_interview_for_week-----------------------
	nh.kernel.Handle("/see_interview_for_week", func(ctx tele.Context) error {
		username := ctx.Chat().Username
		if !slices.Contains(nh.cfg.Whitelist, username) {
			return ctx.Send("You are not in white list!")
		}
		today := time.Now()
		afterOneWeek := today.AddDate(0, 0, 7)

		message, err := nh.useCase.GetInterviewByDate(&today, &afterOneWeek)
		err = ctx.Send(*message, &tele.SendOptions{ParseMode: tele.ModeHTML})
		if err != nil {
			log.Fatal(err)
			return err
		}

		return nil
	})

	//---------------------make_admin_interviewer_by_date-----------------------
	nh.kernel.Handle("/make_admin_interviewer_by_date", func(ctx tele.Context) error {
		username := ctx.Chat().Username
		if !slices.Contains(nh.cfg.Whitelist, username) {
			return ctx.Send("You are not in white list!")
		}

		err := ctx.Send("Введите дату за которую вы хотите назначать собеседования\nВ формате ДД.ММ.ГГГГ ;)")
		if err != nil {
			log.Fatal(err)
			return err
		}

		nh.kernel.Handle(tele.OnText, func(ctx tele.Context) error {
			selectedDateString := strings.Replace(ctx.Text(), "\f", "", 1)
			selectedDateTime, err := time.Parse(layout, selectedDateString)
			if err != nil {
				err = ctx.Send("Неверный формат даты!")
			}

			interviews, err := nh.useCase.GetInterviewerByDate(&selectedDateTime, &selectedDateTime)
			var keys []tele.Row
			var keyboard tele.ReplyMarkup

			for i := 0; i < len(interviews); i += 3 {
				var btn []tele.Btn
				for j := i; j < i+3; j++ {
					if j >= len(interviews) {
						break
					}
					btn = append(btn, keyboard.Data(*interviews[j].CandidateName, *interviews[j].CandidateName))
				}
				keys = append(keys, keyboard.Row(btn[:]...))
			}

			keyboard.Inline(keys[:]...)
			err = ctx.Send("Выберите, кандидата которому хотите назнчить интервьювера:", &keyboard)
			if err != nil {
				log.Fatal(err)
			}

			nh.kernel.Handle(tele.OnCallback, func(ctx tele.Context) error {
				candidate := strings.Replace(ctx.Callback().Data, "\f", "", 1)
				if err != nil {
					log.Fatalf("Error in parse interviewIdString to int type: %v", err)
					return err
				}
				interviewersKeyboard, err := nh.useCase.GetInterviewer()
				if err != nil {
					return err
				}

				if err != nil {
					return err
				}
				err = ctx.Send("Выберите интервьювера!", interviewersKeyboard)
				if err != nil {
					log.Fatal(err)
					return err
				}

				nh.kernel.Handle(tele.OnCallback, func(ctx tele.Context) error {
					interviewer := strings.Replace(ctx.Callback().Data, "\f", "", 1)
					err := nh.useCase.SetInterviewerByName(
						&model.SetInterviewerParams{
							CandidateName: &candidate,
							Interviewer:   &interviewer,
							DateInterview: &selectedDateTime,
						})
					if err != nil {
						return err
					}

					if err != nil {
						return err
					}
					err = ctx.Send(fmt.Sprintf("Кандидату %s успешно назначен интервьювером %s", candidate, interviewer))
					if err != nil {
						log.Fatal(err)
						return err
					}

					return nil
				})

				return nil
			})

			return nil
		})

		return nil
	})

	//---------------------see_interview_for_week_ago-----------------------
	nh.kernel.Handle("/see_interview_for_week_ago", func(ctx tele.Context) error {
		username := ctx.Chat().Username
		if !slices.Contains(nh.cfg.Whitelist, username) {
			return ctx.Send("You are not in white list!")
		}
		today := time.Now()
		afterOneWeek := today.AddDate(0, 0, -7)

		message, err := nh.useCase.GetInterviewByDate(&today, &afterOneWeek)
		err = ctx.Send(*message, &tele.SendOptions{ParseMode: tele.ModeHTML})
		if err != nil {
			log.Fatal(err)
			return err
		}

		return nil
	})
}
