package usecase

import (
	"dnaim_admin/config"
	naimInterface "dnaim_admin/internal/naim/interface"
	"dnaim_admin/internal/naim/model"
	"dnaim_admin/pkg/logger"
	"fmt"
	tele "gopkg.in/telebot.v3"
	"time"
)

type NaimUseCase struct {
	NaimRepo naimInterface.NaimRepositoryI
	logger   logger.Logger
	cfg      *config.Config
}

func NewNaimUseCase(ApiGoRepo naimInterface.NaimRepositoryI, logger logger.Logger, cfg *config.Config) naimInterface.NaimUseCaseI {
	return &NaimUseCase{
		NaimRepo: ApiGoRepo,
		logger:   logger,
		cfg:      cfg,
	}
}

func (nuc *NaimUseCase) createInterviewMessage(params model.GetInterviewByDateParams, i *int64) (message string) {
	var interviewer string
	if *params.Interviewer == "" {
		interviewer = "НЕТ ИНТЕРВЬЮВЕРА!"
	} else {
		interviewer = *params.Interviewer
	}

	message = fmt.Sprintf("%d) Кандидат (ID = %d) на %s: %s на %s в %s у %s - %s\n", *i, *params.InterviewID, *params.Job, *params.CandidateName, *params.DateInterview, *params.Slot, interviewer, *params.Status)

	return
}

func (nuc *NaimUseCase) createInterviewerMessage(params model.GetInterviewByDateParams) (message string) {
	var interviewer string
	if *params.Interviewer == "" {
		interviewer = "НЕТ ИНТЕРВЬЮВЕРА!"
	} else {
		interviewer = *params.Interviewer
	}

	message = fmt.Sprintf(`
ID кандидата: %d
ФИО кандидата: %s
ТГ аккаунт кандидата: %s
Должность: %s
Дата собеседования: %s
Время собеседования: %s
Интервьювер: %s
`, *params.InterviewID, *params.CandidateName, *params.CandidateChatName, *params.Job, *params.DateInterview,
		*params.Slot, interviewer)

	return
}

func (nuc *NaimUseCase) GetInterviewByDate(dateFrom, dateTo *time.Time) (*string, error) {
	result, err := nuc.NaimRepo.GetInterviewByDate(dateFrom, dateTo)
	if err != nil {
		return nil, err
	}

	message := "<b>Информация по собеседованиям:</b>\n"
	for i, data := range result {
		iter := int64(i + 1)
		message += nuc.createInterviewMessage(data, &iter)
	}

	return &message, nil
}

func (nuc *NaimUseCase) GetInterviewerByDate(dateFrom, dateTo *time.Time) ([]model.GetInterviewByDateParams, error) {
	result, err := nuc.NaimRepo.GetInterviewByDate(dateFrom, dateTo)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (nuc *NaimUseCase) SetInterviewerByDate(params *model.SetInterviewerParams) error {
	if err := nuc.NaimRepo.SetInterviewer(params); err != nil {
		return err
	}

	return nil
}

func (nuc *NaimUseCase) GetInterviewByID(id *int64) (*string, error) {
	result, err := nuc.NaimRepo.GetInterviewByID(id)
	if err != nil {
		return nil, err
	}

	message := "Информация по собеседованию:\n"
	message += nuc.createInterviewerMessage(*result)

	return &message, nil
}

func (nuc *NaimUseCase) GetStatus() ([]model.GetStatusResult, error) {
	result, err := nuc.NaimRepo.GetStatus()
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (nuc *NaimUseCase) SetStatus(params *model.SetStatusParams) error {
	if err := nuc.NaimRepo.SetStatus(params); err != nil {
		return err
	}

	return nil
}

func (nuc *NaimUseCase) GetInterviewer() (*tele.ReplyMarkup, error) {
	result, err := nuc.NaimRepo.GetInterviewer()
	if err != nil {
		return nil, err
	}

	var keys []tele.Row
	var keyboard tele.ReplyMarkup

	for i := 0; i < len(result); i += 3 {
		var btn []tele.Btn
		for j := i; j < i+3; j++ {
			if j >= len(result) {
				break
			}
			btn = append(btn, keyboard.Data(*result[j].Name, *result[j].Name))
		}
		keys = append(keys, keyboard.Row(btn[:]...))
	}

	keyboard.Inline(keys[:]...)

	return &keyboard, nil
}

func (nuc *NaimUseCase) SetInterviewerByName(params *model.SetInterviewerParams) error {
	if err := nuc.NaimRepo.SetInterviewerByName(params); err != nil {
		return err
	}

	return nil
}
