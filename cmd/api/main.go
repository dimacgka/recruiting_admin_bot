package main

import (
	"dnaim_admin/config"
	"dnaim_admin/internal/pkg/bot/connection"
)

func main() {
	config := config.NewConfig()
	_ = connection.NewTelegramBot(config)
}
