package headers

import (
	"crypto/hmac"
	"crypto/sha512"
	"encoding/hex"
	"strconv"
	"time"
)

// MakeAuthHeaders Function that build default auth headers to work with other services
func MakeAuthHeaders(body, privateKey, publicKey string) (headers map[string]string, err error) {
	headers = make(map[string]string)
	mac := hmac.New(sha512.New, []byte(privateKey))
	timeStamp := time.Now().Unix()
	timeStampString := strconv.Itoa(int(timeStamp))
	mac.Write([]byte(timeStampString + body))
	signature := hex.EncodeToString(mac.Sum(nil))
	headers["Content-Type"] = "application/json"
	headers["ApiPublic"] = publicKey
	headers["Signature"] = signature
	headers["TimeStamp"] = timeStampString

	return headers, nil
}
