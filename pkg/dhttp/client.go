package dhttp

import (
	"dnaim_admin/config"
	"dnaim_admin/pkg/logger"
	"fmt"
	"github.com/google/uuid"
	"github.com/valyala/fasthttp"
	http "github.com/valyala/fasthttp"
	"time"
)

type DHttpClient struct {
	httpClient *http.Client
	Config     *config.Config
	Logger     logger.Logger
}

func NewHttpClient(cfg *config.Config, logger *logger.Logger) *DHttpClient {
	httpClient := http.Client{
		Name:         "StatisticBot",
		ReadTimeout:  time.Duration(cfg.Http.TimeOut) * time.Second,
		WriteTimeout: time.Duration(cfg.Http.TimeOut) * time.Second,
	}

	return &DHttpClient{
		Config:     cfg,
		Logger:     *logger,
		httpClient: &httpClient,
	}
}

func (hc *DHttpClient) MakeQueryString(
	queryParams map[string]interface{},
) (query string) {
	var queryCount int

	for k, v := range queryParams {
		switch queryCount {
		case 0:
			query += fmt.Sprintf("?%s=%s", k, v)
		default:
			query += fmt.Sprintf("&%s=%s", k, v)
		}
		queryCount++
	}

	return query
}

func (hc *DHttpClient) logRequest(req *http.Request, res *http.Response) {
	logMessage := fmt.Sprintf("REQUEST:\n %s\nRESPONSE:\n %s", req.Body(), res.Body())
	statusGroup := GetCodeGroup(res.StatusCode())
	switch statusGroup {
	case ClientError:
		hc.Logger.Warnf(logMessage)
		break
	case ServerError:
		hc.Logger.Errorf(logMessage)
		break
	default:
		hc.Logger.Infof(logMessage)
		break
	}
}

func (hc *DHttpClient) Request(method string, url string, body []byte, queryParams map[string]string, headers map[string]string) (responseBody []byte, statusCode int, err error) {
	req := fasthttp.AcquireRequest()
	req.Header.SetMethod(method)
	scenarioName := "fullOK"
	var timeout = time.Millisecond * 5000
	requestSerialID := uuid.New().String()
	for k, v := range headers {
		req.Header.Set(k, v)
	}
	var queryCount int
	for k, v := range queryParams {
		switch queryCount {
		case 0:
			url += fmt.Sprintf("?%s=%s", k, v)
		default:
			url += fmt.Sprintf("&%s=%s", k, v)
		}
		queryCount++
	}
	req.Header.Set("requestSerialID", requestSerialID)
	req.Header.Set("scenarioName", scenarioName)

	req.SetBody(body)
	req.SetRequestURI(url)
	res := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseRequest(req)
	defer fasthttp.ReleaseResponse(res)

	client := &fasthttp.Client{}

	if err = client.DoTimeout(req, res, timeout); err != nil {
		return
	}

	statusCode = res.StatusCode()

	responseBody = make([]byte, len(res.Body()))
	copy(responseBody, res.Body())
	req.SetConnectionClose()

	hc.logRequest(req, res)

	return
}
