package logger

import (
	"dnaim_admin/config"
	"fmt"
	"github.com/afiskon/promtail-client/promtail"
	"github.com/sirupsen/logrus"
	"time"
)

const (
	labels string = "source=%s job=error_logging statusCode=%d internalError=%s requestId=%s"
)

type LogLevel uint8

const (
	DEBUG LogLevel = iota
	INFO
	WARN
	ERROR
)

type Logger struct {
	Config *config.Config
	loki   promtail.Client
	logger *logrus.Logger
}

func InitLogger(cfg *config.Config) *Logger {
	logger := logrus.New()
	logger.Formatter = &logrus.TextFormatter{
		FullTimestamp: true,
	}

	loki, err := promtail.NewClientProto(promtail.ClientConfig{
		PushURL: fmt.Sprintf("http://%s:%s",
			cfg.Logger.Loki.Host, cfg.Logger.Loki.Port),
		SendLevel:          promtail.INFO,
		PrintLevel:         promtail.DISABLE,
		BatchEntriesNumber: cfg.Logger.Loki.Batch.Number,
		BatchWait:          time.Duration(cfg.Logger.Loki.Batch.Wait) * time.Second,
		Labels: fmt.Sprintf("{source=\"%s\"}",
			cfg.Base.Service.Name),
	})
	if err != nil {
		return nil
	}
	return &Logger{
		Config: cfg,
		logger: logger,
		loki:   loki,
	}
}

func (L *Logger) log(level logrus.Level, msg string, args ...interface{}) {
	L.logger.Logf(level, msg, args...)
}

func (L *Logger) sendLog(msg string, level LogLevel) {
	switch level {
	case ERROR:
		L.loki.Errorf(msg)
		break
	case WARN:
		L.loki.Warnf(msg)
		break
	case DEBUG:
		L.loki.Debugf(msg)
		break
	default:
		L.loki.Infof(msg)
	}
}

func (L *Logger) Infof(msgf string, args ...interface{}) {
	L.log(logrus.InfoLevel, msgf, args...)
}

func (L *Logger) Debugf(msgf string, args ...interface{}) {
	L.log(logrus.DebugLevel, msgf, args...)
}

func (L *Logger) Warnf(msgf string, args ...interface{}) {
	L.log(logrus.WarnLevel, msgf, args...)
}

func (L *Logger) Errorf(msgf string, args ...interface{}) {
	L.log(logrus.ErrorLevel, msgf, args...)
}

func (L *Logger) ErrorFull(err error, requestId string) {
	go L.sendLog(fmt.Sprintf(labels,
		L.Config.Base.Service.Name,
		500, err.Error(), requestId),
		ERROR)

	L.Errorf("Error: %s", err.Error())
}

// Write implementing for putting it into fiber logger
func (L *Logger) Write(p []byte) (n int, err error) {

	return len(p), nil
}
