package relational

import (
	"dnaim_admin/config"
	"dnaim_admin/pkg/logger"
	"fmt"
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	"time"
)

func InitPostgres(cfg *config.Config, logger *logger.Logger) *sqlx.DB {
	connectionUrl := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.DBName,
		cfg.Postgres.SSLMode,
	)
	database, err := sqlx.Connect(cfg.Postgres.PgDriver, connectionUrl)
	if err != nil {
		panic(fmt.Errorf("can't connected to DB: %s", err.Error()))
	}

	database.DB.SetConnMaxIdleTime(time.Duration(cfg.Postgres.ConnMaxIdleTime) * time.Second)
	database.DB.SetMaxOpenConns(cfg.Postgres.MaxOpenConns)

	logger.Infof("Connected to PostgreSQL!")

	return database
}
