package config

import (
	botConfig "dnaim_admin/internal/pkg/bot"
	baseConfig "dnaim_admin/pkg/config"
	dHttpConfig "dnaim_admin/pkg/dhttp/config"
	loggerConfig "dnaim_admin/pkg/logger/config"
	dStorageConfig "dnaim_admin/pkg/storage/config"
	"fmt"
	"github.com/spf13/viper"
	"log"
)

type Config struct {
	Postgres  dStorageConfig.PostgresConfig
	Logger    loggerConfig.LoggerConfig
	Base      baseConfig.BaseConfig
	Http      dHttpConfig.DHttpConfig
	Bot       botConfig.BotConfig
	Whitelist []string
}

func NewConfig() *Config {
	v, err := loadConfig()
	if err != nil {
		panic(fmt.Errorf("can't load config: %s", err.Error()))
	}

	config, err := parseConfig(v)
	if err != nil {
		panic(fmt.Errorf("can't parse config: %s", err.Error()))
	}

	log.Println("Config is loaded!")

	return config
}

func loadConfig() (*viper.Viper, error) {
	v := viper.New()
	v.AddConfigPath("config")
	v.SetConfigName("config")
	v.SetConfigType("yml")
	err := v.ReadInConfig()
	if err != nil {
		return nil, err
	}
	return v, nil
}

func parseConfig(v *viper.Viper) (*Config, error) {
	var c Config
	err := v.Unmarshal(&c)
	if err != nil {
		log.Fatalf("unable to decode config into struct, %v", err)
		return nil, err
	}
	return &c, nil
}
